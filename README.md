# Guides

Welcome to Ipsos Advanced Analytics (formerly Ipsos Science Center)! This repository holds useful information for our group members. The documents in this repo cover these main topics:

- Access information for Ipsos resources:
	- GCP
	- Shared I drive
	- etc
- Coding guidelines for major languages used by the tam
- Project guidelines
	- Proper folder organization
	- Proper git use
	- Helpful tools 